use num_derive::FromPrimitive;

mod header; pub use header::*;
mod message; pub use message::*;
mod domain; pub use domain::*;
mod query; pub use query::*;
mod record; pub use record::*;

#[derive(Clone, Copy, Debug, PartialEq, FromPrimitive)]
pub enum OpCode {
    Query,
    Status,
    Notify,
    Update
}

#[derive(Clone, Copy, Debug, PartialEq, FromPrimitive)]
pub enum ResponseCode {
    NoError = 0,
    FormErr = 1,
    ServFail = 2,
    NXDomain = 3,
    NotImp = 4,
    Refused = 5
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Class {
    IN,
    CH,
    HS,
    NONE,
    ANY,
    OPT(u16)
}

impl Class {
    pub fn from_u16(class: u16) -> Option<Self> {
        Some(match class {
            1 => Class::IN,
            3 => Class::CH,
            4 => Class::HS,
            254 => Class::NONE,
            255 => Class::ANY,
            _ => return None
        })
    }

    pub fn to_u16(self) -> u16 {
        match self {
            Class::IN => 1,
            Class::CH => 3,
            Class::HS => 4,
            Class::NONE => 254,
            Class::ANY => 255,
            Class::OPT(x) => x.max(512)
        }
    }
}
