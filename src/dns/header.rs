use super::OpCode;
use byteorder::{ReadBytesExt, WriteBytesExt, BE};
use num_traits::FromPrimitive;

#[derive(Clone, Debug, PartialEq)]
pub struct Header {
    pub id: u16,
    pub is_query: bool,
    pub op_code: OpCode,
    pub authoritative: bool,
    pub truncated: bool,
    pub recursion_desired: bool,
    pub recursion_available: bool,
    pub authentic_data: bool,
    pub checking_disabled: bool,
    pub response_code: u8,
    pub query_count: u16,
    pub answer_count: u16,
    pub authority_count: u16,
    pub additional_count: u16,
}

impl Default for Header {
    fn default() -> Self {
        Self {
            id: 0,
            is_query: true,
            op_code: OpCode::Query,
            authoritative: false,
            truncated: false,
            recursion_desired: true,
            recursion_available: false,
            authentic_data: false,
            checking_disabled: false,
            response_code: 0,
            query_count: 0,
            answer_count: 0,
            authority_count: 0,
            additional_count: 0,
        }
    }
}

impl Header {
    pub fn read<R: ReadBytesExt>(buf: &mut R) -> Option<Self> {
        let id = buf.read_u16::<BE>().ok()?;
        let flags = buf.read_u16::<BE>().ok()?;
        Some(Self {
            id,
            is_query: flags >> 15 == 0,
            op_code: OpCode::from_u16((flags >> 11) & 0xf)?,
            authoritative: (flags >> 10) & 1 == 1,
            truncated: (flags >> 9) & 1 == 1,
            recursion_desired: (flags >> 8) & 1 == 1,
            recursion_available: (flags >> 7) & 1 == 1,
            authentic_data: (flags >> 5) & 1 == 1,
            checking_disabled: (flags >> 4) & 1 == 1,
            response_code: (flags & 0xf) as u8,
            query_count: buf.read_u16::<BE>().ok()?,
            answer_count: buf.read_u16::<BE>().ok()?,
            authority_count: buf.read_u16::<BE>().ok()?,
            additional_count: buf.read_u16::<BE>().ok()?,
        })
    }

    pub fn write(&self, buf: &mut Vec<u8>) {
        let mut buf = std::io::Cursor::new(buf);
        let mut flags = (!self.is_query as u16) << 15;

        flags |= (self.op_code.clone() as u16) << 11;
        flags |= (self.authoritative as u16) << 10;
        flags |= (self.truncated as u16) << 9;
        flags |= (self.recursion_desired as u16) << 8;
        flags |= (self.recursion_available as u16) << 7;
        flags |= (self.authentic_data as u16) << 5;
        flags |= (self.checking_disabled as u16) << 4;
        flags |= self.response_code as u16;

        buf.write_u16::<BE>(self.id).unwrap();
        buf.write_u16::<BE>(flags).unwrap();
        buf.write_u16::<BE>(self.query_count).unwrap();
        buf.write_u16::<BE>(self.answer_count).unwrap();
        buf.write_u16::<BE>(self.authority_count).unwrap();
        buf.write_u16::<BE>(self.additional_count).unwrap();
    }
}
