use super::record::RecordType;
use super::{Class, Label, Name};
use byteorder::{ReadBytesExt, WriteBytesExt, BE};
use num_traits::FromPrimitive;
use std::collections::HashMap;
use std::io::Cursor;

#[derive(Clone, Debug, PartialEq)]
pub struct Query {
    pub name: Name,
    pub record_type: RecordType,
    pub class: Class,
}

impl Query {
    pub fn new<N: Into<Name>>(name: N, record_type: RecordType) -> Self {
        Self {
            name: name.into(),
            record_type,
            class: Class::IN,
        }
    }

    pub fn read<R: AsRef<[u8]>>(
        buf: &mut Cursor<R>,
        label_map: &mut HashMap<usize, Label>,
    ) -> Option<Self> {
        Some(Self {
            name: Name::read(buf, label_map)?,
            record_type: RecordType::from_u16(buf.read_u16::<BE>().ok()?)?,
            class: Class::from_u16(buf.read_u16::<BE>().ok()?)?,
        })
    }

    pub fn write(&self, buf: &mut Vec<u8>, pointer_map: &mut HashMap<Label, (usize, usize)>) {
        &self.name.write(buf, pointer_map);
        buf.write_u16::<BE>(self.record_type.clone() as u16)
            .unwrap();
        buf.write_u16::<BE>(self.class.to_u16()).unwrap();
    }
}
