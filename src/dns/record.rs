use super::{Class, Label, Name};
use byteorder::{ReadBytesExt, WriteBytesExt, BE};
use num_derive::FromPrimitive;
use num_traits::FromPrimitive;
use std::collections::HashMap;
use std::io::{Cursor, Read, Write};

#[derive(Clone, Copy, Debug, PartialEq, FromPrimitive)]
pub enum RecordType {
    A = 1,
    NS = 2,
    CNAME = 5,
    SOA = 6,
    NULL = 10,
    PTR = 12,
    TXT = 16,
    AAAA = 28,
    OPT = 41,
    ANY = 255,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Record {
    pub name: Name,
    pub record_type: RecordType,
    pub class: Class,
    pub ttl: u32,
    pub data: Vec<u8>,
}

impl Record {
    pub fn new<N: Into<Name>>(name: N, record_type: RecordType, ttl: u32, data: Vec<u8>) -> Self {
        Self {
            name: name.into(),
            record_type,
            class: Class::IN,
            ttl,
            data,
        }
    }

    pub fn read<A: AsRef<[u8]>>(
        buf: &mut Cursor<A>,
        label_map: &mut HashMap<usize, Label>,
    ) -> Option<Self> {
        let name = Name::read(buf, label_map)?;
        let record_type = RecordType::from_u16(buf.read_u16::<BE>().ok()?)?;
        let class = if record_type == RecordType::OPT {
            Class::OPT(buf.read_u16::<BE>().ok()?)
        } else {
            Class::from_u16(buf.read_u16::<BE>().ok()?)?
        };
        let ttl = buf.read_u32::<BE>().ok()?;
        let len = buf.read_u16::<BE>().ok()?;
        let mut data = vec![0; len as usize];
        buf.read_exact(&mut data).ok()?;
        Some(Self {
            name,
            record_type,
            class,
            ttl,
            data,
        })
    }

    pub fn write(&self, buf: &mut Vec<u8>, pointer_map: &mut HashMap<Label, (usize, usize)>) {
        self.name.write(buf, pointer_map);
        buf.write_u16::<BE>(self.record_type.clone() as u16)
            .unwrap();
        buf.write_u16::<BE>(self.class.to_u16()).unwrap();
        buf.write_u32::<BE>(self.ttl).unwrap();
        buf.write_u16::<BE>(self.data.len() as u16).unwrap();
        buf.write(&self.data).unwrap();
    }
}
