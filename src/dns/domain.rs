use byteorder::{ReadBytesExt, WriteBytesExt, BE};
use std::collections::HashMap;
use std::io::{Cursor, Read, Write};
use std::rc::Rc;

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct Label(Rc<[u8]>);

impl Label {
    pub fn new<I: Into<Label>>(label: I) -> Self {
        label.into()
    }
}

impl Into<Label> for String {
    fn into(self) -> Label {
        Label(Rc::from(self.into_bytes()))
    }
}

impl Into<Label> for &str {
    fn into(self) -> Label {
        Label(Rc::from(self.as_bytes()))
    }
}

impl Into<Label> for Vec<u8> {
    fn into(self) -> Label {
        Label(Rc::from(self))
    }
}

impl Into<Label> for &[u8] {
    fn into(self) -> Label {
        Label(Rc::from(self))
    }
}

impl AsRef<[u8]> for Label {
    fn as_ref(&self) -> &[u8] {
        &self.0
    }
}

impl std::borrow::Borrow<[u8]> for Label {
    fn borrow(&self) -> &[u8] {
        &self.0
    }
}

impl std::fmt::Display for Label {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for &c in self.0.iter() {
            if c > 32 && c < 128 && c != 46 {
                if "\"\\".chars().any(|x| x == c as char) {
                    write!(f, "\\")?
                }
                write!(f, "{}", c as char)?;
                continue;
            }
            write!(f, "\\x{:02x}", c)?
        }
        Ok(())
    }
}

impl std::fmt::Debug for Label {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "\"{}\"", self)
    }
}

impl std::ops::Add<Name> for Label {
    type Output = Name;
    fn add(self, mut rhs: Name) -> Self::Output {
        rhs.0.insert(0, self);
        rhs
    }
}

#[derive(Clone, Debug, PartialEq, Hash)]
pub struct Name(Vec<Label>);

impl Name {
    pub fn new<I: Into<Self>>(a: I) -> Self {
        a.into()
    }

    pub fn from_labels<I: Into<Label>, A: IntoIterator<Item = I>>(labels: A) -> Self {
        Self(labels.into_iter().map(|l| l.into()).collect())
    }

    pub fn labels(&self) -> &[Label] {
        &self.0
    }

    pub fn len(&self) -> usize {
        self.0.iter().fold(0, |acc, x| acc + x.0.len()) + self.0.len() + 1
    }

    pub fn zone_of(&self, zone: &Self) -> bool {
        if self.0.len() < zone.0.len() {
            return false;
        }
        for (a, b) in self.0.iter().rev().zip(zone.0.iter().rev()) {
            if a != b {
                return false;
            }
        }
        true
    }

    pub fn read<A: AsRef<[u8]>>(
        buf: &mut Cursor<A>,
        label_map: &mut HashMap<usize, Label>,
    ) -> Option<Self> {
        let mut labels = Vec::new();
        let mut end = 0;
        loop {
            let len = buf.read_u8().ok()?;
            if len == 0 {
                break;
            }

            if len & 0xc0 == 0xc0 {
                let offset = ((len & 0x3f) as u64) << 8 | buf.read_u8().ok()? as u64;
                if offset >= buf.position() - 1 {
                    return None;
                }
                if end == 0 {
                    end = buf.position()
                }
                buf.set_position(offset);
            } else if len & 0xc0 == 0 {
                labels.push(
                    if let Some(label) = label_map.get(&(buf.position() as usize)) {
                        label.clone()
                    } else {
                        let mut slice = vec![0; len as usize];
                        buf.read_exact(&mut slice).ok()?;
                        let label: Label = String::from_utf8(slice).ok()?.into();
                        label_map.insert(buf.position() as usize, label.clone());
                        label
                    },
                );
            } else {
                return None;
            }
        }
        if end != 0 {
            buf.set_position(end)
        }
        Some(Self(labels))
    }

    pub fn write(&self, buf: &mut Vec<u8>, pointer_map: &mut HashMap<Label, (usize, usize)>) {
        for (i, label) in self.0.iter().enumerate() {
            let last = self.0.last().unwrap();
            let mut last_end = 0;
            let mut pointer = 0;

            for (i, label) in self.0[i..].iter().enumerate() {
                if let Some(&(start, end)) = pointer_map.get(label) {
                    if i == 0 || last_end == start {
                        if i == 0 {
                            pointer = start as u16
                        }

                        if label == last {
                            buf.write_u16::<BE>(pointer | 0xc000).unwrap();
                            return;
                        }

                        last_end = end
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }

            pointer_map.insert(label.clone(), (buf.len(), buf.len() + label.0.len() + 1));
            buf.write_u8(label.0.len() as u8).unwrap();
            buf.write(&label.0).unwrap();
        }
        buf.write_u8(0).unwrap();
    }
}

impl std::ops::Add<Name> for Name {
    type Output = Name;
    fn add(mut self, rhs: Name) -> Self {
        for label in rhs.0 {
            self.0.push(label)
        }
        self
    }
}

impl std::ops::Add<Label> for Name {
    type Output = Name;
    fn add(mut self, rhs: Label) -> Self {
        self.0.push(rhs);
        self
    }
}

impl Into<Name> for &str {
    fn into(self) -> Name {
        Name(self.split(".").map(|s| s.into()).collect::<Vec<_>>())
    }
}

impl Into<Name> for String {
    fn into(self) -> Name {
        self[..].into()
    }
}

impl Into<Name> for Vec<Label> {
    fn into(self) -> Name {
        Name(self)
    }
}

impl std::fmt::Display for Name {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for (i, label) in self.0.iter().enumerate() {
            if i > 0 {
                f.write_str(".")?
            }
            write!(f, "{}", label)?
        }
        Ok(())
    }
}
