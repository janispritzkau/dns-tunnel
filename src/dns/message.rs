use super::{Header, Query, Record};

#[derive(Clone, Debug, PartialEq, Default)]
pub struct Message {
    pub header: Header,
    pub queries: Vec<Query>,
    pub answers: Vec<Record>,
    pub authorities: Vec<Record>,
    pub additionals: Vec<Record>,
}

impl Message {
    pub fn new(id: u16, is_query: bool) -> Self {
        Self {
            header: Header {
                id,
                is_query,
                ..Default::default()
            },
            ..Default::default()
        }
    }

    pub fn add_query(&mut self, query: Query) -> &mut Self {
        self.queries.push(query);
        self.header.query_count += 1;
        self
    }

    pub fn add_answer(&mut self, record: Record) -> &mut Self {
        self.answers.push(record);
        self.header.answer_count += 1;
        self
    }

    pub fn add_authority(&mut self, record: Record) -> &mut Self {
        self.authorities.push(record);
        self.header.authority_count += 1;
        self
    }

    pub fn add_additional(&mut self, record: Record) -> &mut Self {
        self.additionals.push(record);
        self.header.additional_count += 1;
        self
    }

    pub fn to_bytes(&self) -> Option<Vec<u8>> {
        let mut buf = Vec::with_capacity(512);

        let header = Header {
            query_count: self.queries.len() as u16,
            answer_count: self.answers.len() as u16,
            authority_count: self.authorities.len() as u16,
            additional_count: self.additionals.len() as u16,
            ..self.header.clone()
        };

        header.write(&mut buf);

        let mut pointer_map = Default::default();

        for query in &self.queries {
            query.write(&mut buf, &mut pointer_map)
        }
        for answer in &self.answers {
            answer.write(&mut buf, &mut pointer_map)
        }
        for authority in &self.authorities {
            authority.write(&mut buf, &mut pointer_map)
        }
        for additional in &self.additionals {
            additional.write(&mut buf, &mut pointer_map)
        }

        Some(buf)
    }

    pub fn from_bytes(buf: &[u8]) -> Option<Message> {
        let mut cursor = std::io::Cursor::new(buf);
        let header = Header::read(&mut cursor)?;

        let mut queries = vec![];
        let mut answers = vec![];
        let mut authorities = vec![];
        let mut additionals = vec![];

        let mut label_map = Default::default();

        for _ in 0..header.query_count {
            queries.push(Query::read(&mut cursor, &mut label_map)?)
        }
        for _ in 0..header.answer_count {
            answers.push(Record::read(&mut cursor, &mut label_map)?)
        }
        for _ in 0..header.authority_count {
            authorities.push(Record::read(&mut cursor, &mut label_map)?)
        }
        for _ in 0..header.additional_count {
            additionals.push(Record::read(&mut cursor, &mut label_map)?)
        }

        Some(Self {
            header,
            queries,
            answers,
            authorities,
            additionals,
        })
    }
}

impl From<Header> for Message {
    fn from(header: Header) -> Self {
        Self {
            header,
            ..Default::default()
        }
    }
}
